package com.aias.fun.ratelimit;

import com.alibaba.csp.sentinel.slots.block.BlockException;

/**
 * @author liuhy
 * @since 2021/5/27
 */
public class CustomerExceptionHandler {
    public String handleException(BlockException exception) {
        return "自定义限流信息: " + exception.getClass().getName();
    }
}
