package com.aias.fun.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @author liuhy
 * @since 2021/5/27
 */
@RestController
@RequestMapping("/breaker")
public class CircleBreakerController {
    @Resource
    private RestTemplate restTemplate;
    @Value("${service-url.provider}")
    private String providerUrl;

    @RequestMapping("/fallback/{name}")
    @SentinelResource(value = "fallback", fallback = "handleFallback")
    public String fallback(@PathVariable String name) {
        return restTemplate.getForObject(providerUrl + "/sayHello/{1}", String.class, name);
    }

    @RequestMapping("/fallbackException/{name}")
    @SentinelResource(value = "fallbackException", fallback = "handleFallback2", exceptionsToIgnore =
            {NullPointerException.class})
    public String fallbackException(@PathVariable String name) {
        if ("zhangsan".equals(name)) {
            throw new IndexOutOfBoundsException();
        } else if ("lisi".equals(name)) {
            throw new NullPointerException();
        }
        return restTemplate.getForObject(providerUrl + "/sayHello/{1}", String.class, name);
    }

    public String handleFallback(String name) {
        return "handleFallback降级服务返回: " + name;
    }

    public String handleFallback2(@PathVariable String name, Throwable e) {
        return "handleFallback降级服务返回: " + name + " exception: " + e.getClass().getName();
    }
}
