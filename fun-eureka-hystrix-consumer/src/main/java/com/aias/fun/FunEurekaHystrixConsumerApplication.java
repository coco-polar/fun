package com.aias.fun;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author liuhy
 * @since 2021/5/18
 */
@SpringCloudApplication
@EnableDiscoveryClient
@EnableCircuitBreaker
public class FunEurekaHystrixConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(FunEurekaHystrixConsumerApplication.class, args);
    }
}
