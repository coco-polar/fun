package com.aias.fun.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @author liuhy
 * @since 2021/5/20
 */
@RestController
public class ConsumerController {

    @Resource
    private RestTemplate restTemplate;

    @GetMapping(value = "/hello/{name}")
    @HystrixCommand(fallbackMethod = "returnDefaultString")
    public String hello(@PathVariable("name") String name) {
        String url = "http://fun-eureka-provider/sayHello/" + name;
        String msg = restTemplate.getForEntity(url, String.class).getBody();
        return "返回信息为:" + msg;
    }

    public String returnDefaultString(@PathVariable("name")String name) {
        return "返回信息为: 调用失败,返回默认字符串" + name;
    }
}
