package com.aias.fun;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.turbine.EnableTurbine;

/**
 * @author liuhy
 * @since 2021/5/18
 */
@SpringCloudApplication
@EnableDiscoveryClient
@EnableTurbine
public class FunEurekaTurbineApplication {
    public static void main(String[] args) {
        SpringApplication.run(FunEurekaTurbineApplication.class, args);
    }
}
