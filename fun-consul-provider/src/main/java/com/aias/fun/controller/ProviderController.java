package com.aias.fun.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liuhy
 * @since 2021/5/20
 */
@RestController
public class ProviderController {
    @Value("${server.port}")
    private String port;

    @GetMapping("/sayHello/{name}")
    public String sayHello(@PathVariable("name") String name) {
        return "hello from： " + port + " to " + name;
    }
}
