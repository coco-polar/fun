package com.aias.fun;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * @author liuhy
 * @since 2021/5/18
 */
@SpringCloudApplication
@EnableDiscoveryClient
@EnableHystrixDashboard
public class FunEurekaHystrixDashBoardApplication {
    public static void main(String[] args) {
        SpringApplication.run(FunEurekaHystrixDashBoardApplication.class, args);
    }
}
