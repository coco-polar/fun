package com.aias.fun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * @author liuhy
 * @since 2021/5/18
 */
@SpringCloudApplication
@EnableOAuth2Sso
public class FunSecurityClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(FunSecurityClientApplication.class, args);
    }
}
