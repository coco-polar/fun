package com.aias.fun;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author liuhy
 * @since 2021/5/18
 */
@SpringCloudApplication
@EnableDiscoveryClient
@EnableFeignClients
public class FunEurekaFeignConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(FunEurekaFeignConsumerApplication.class, args);
    }
}
