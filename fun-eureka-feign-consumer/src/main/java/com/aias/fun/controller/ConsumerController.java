package com.aias.fun.controller;

import com.aias.fun.api.ProviderApi;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @author liuhy
 * @since 2021/5/20
 */
@RestController
public class ConsumerController {

    @Resource
    private ProviderApi providerApi;

    @GetMapping(value = "/hello/{name}")
    public String hello(@PathVariable("name") String name) {
        return providerApi.sayHello(name);
    }
}
