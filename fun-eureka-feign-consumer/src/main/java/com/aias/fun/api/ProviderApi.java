package com.aias.fun.api;

import com.aias.fun.api.fallback.ProviderFallbackService;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author liuhy
 * @since 2021/5/20
 */
@FeignClient(value = "fun-eureka-provider",fallback = ProviderFallbackService.class)
public interface ProviderApi {

    @GetMapping("/sayHello/{name}")
    String sayHello(@PathVariable("name") String name);
}
