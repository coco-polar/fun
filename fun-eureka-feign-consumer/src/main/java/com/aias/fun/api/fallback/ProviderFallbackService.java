package com.aias.fun.api.fallback;

import com.aias.fun.api.ProviderApi;
import org.springframework.stereotype.Component;

/**
 * @author liuhy
 * @since 2021/5/24
 */
@Component
public class ProviderFallbackService implements ProviderApi {
    @Override
    public String sayHello(String name) {
        return "调用失败,返回默认打招呼: "+name;
    }
}
