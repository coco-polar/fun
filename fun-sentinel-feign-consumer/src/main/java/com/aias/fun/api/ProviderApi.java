package com.aias.fun.api;

import com.aias.fun.api.impl.ProviderFallbackService;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author liuhy
 * @since 2021/5/27
 */
@FeignClient(value = "fun-nacos-provider",fallback = ProviderFallbackService.class)
public interface ProviderApi {

    @GetMapping("/sayHello/{name}")
    String sayHello(@PathVariable("name") String name);
}
