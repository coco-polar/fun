package com.aias.fun.controller;

import com.aias.fun.api.ProviderApi;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author liuhy
 * @since 2021/5/27
 */
@RestController
public class ProviderFeignController {
    @Resource
    private ProviderApi providerApi;

    @GetMapping(value = "/hello/{name}")
    public String hello(@PathVariable("name") String name) {
        return providerApi.sayHello(name);
    }
}
