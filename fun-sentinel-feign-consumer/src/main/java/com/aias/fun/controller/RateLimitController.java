package com.aias.fun.controller;

import com.aias.fun.ratelimit.CustomerExceptionHandler;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liuhy
 * @since 2021/5/27
 */
@RestController
@RequestMapping("/rateLimit")
public class RateLimitController {
    /**
     * 按资源名称限流，需要指定限流处理逻辑
     */
    @GetMapping("/byResource")
    @SentinelResource(value = "byResource", blockHandler = "handleException")
    public String byResource() {
        return "按资源名称限流 success";
    }

    /**
     * 按URL限流，有默认的限流处理逻辑
     */
    @GetMapping("/byUrl")
    @SentinelResource(value = "byUrl", blockHandler = "handleException")
    public String byUrl() {
        return "按url限流 success";
    }

    public String handleException(BlockException exception) {
        return exception.getClass().getName() + " exception";
    }

    /**
     * 自定义限流信息
     */
    @GetMapping("/custom")
    @SentinelResource(value = "custom", exceptionsToTrace = {BlockException.class}, blockHandler = "handleException",
            blockHandlerClass = CustomerExceptionHandler.class)
    public String custom() {
        return "自定义限流信息 success";
    }

}
