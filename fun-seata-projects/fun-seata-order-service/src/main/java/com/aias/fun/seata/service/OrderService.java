package com.aias.fun.seata.service;

import com.aias.fun.seata.entity.Order;

public interface OrderService {

    /**
     * 创建订单
     */
    void create(Order order);
}
