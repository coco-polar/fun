package com.aias.fun.seata.mapper;

import com.aias.fun.seata.entity.Order;
import org.apache.ibatis.annotations.Param;


public interface OrderMapper {

    /**
     * 创建订单
     */
    void create(Order order);

    /**
     * 修改订单金额
     */
    void update(@Param("userId") Long userId, @Param("status") Integer status);
}
