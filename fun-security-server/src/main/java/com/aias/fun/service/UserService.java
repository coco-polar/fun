package com.aias.fun.service;

import com.aias.fun.entity.User;
import com.google.common.collect.Lists;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author liuhy
 * @since 2021/5/25
 */
@Service
public class UserService implements UserDetailsService {
    private List<User> userList;
    @Resource
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    public void initData() {
        String password = passwordEncoder.encode("123456");
        userList = Lists.newArrayList();
        userList.add(new User("zhangsan", password, AuthorityUtils.commaSeparatedStringToAuthorityList("admin")));
        userList.add(new User("lisi", password, AuthorityUtils.commaSeparatedStringToAuthorityList("client")));
        userList.add(new User("wangwu", password, AuthorityUtils.commaSeparatedStringToAuthorityList("client")));
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        List<User> findUserList =
                userList.stream().filter(user -> user.getUsername().equals(username)).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(findUserList)) {
            throw new UsernameNotFoundException("用户名或密码错误");
        }
        return findUserList.get(0);
    }
}
