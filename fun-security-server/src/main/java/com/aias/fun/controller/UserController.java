package com.aias.fun.controller;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liuhy
 * @since 2021/5/25
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @GetMapping("/current")
    public Object getCurrentUser(Authentication authentication) {
        return authentication.getPrincipal();
    }
}
