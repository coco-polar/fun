package com.aias.fun;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author liuhy
 * @since 2021/5/18
 */
@SpringCloudApplication
@EnableDiscoveryClient
public class FunNacosProviderApplication {
    public static void main(String[] args) {
        SpringApplication.run(FunNacosProviderApplication.class, args);
    }
}
