package com.aias.fun.controller;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author liuhy
 * @since 2021/5/21
 */
@RestController
public class DiscoveryClientController {

    @Resource
    private DiscoveryClient discoveryClient;

    @GetMapping("/getInstances/{serviceId}")
    public List<ServiceInstance> getInstances(@PathVariable("serviceId") String serviceId) {
        List<ServiceInstance> list = discoveryClient.getInstances(serviceId);
        System.out.println("--------------------------------------");
        if (CollectionUtils.isEmpty(list)) {
            System.out.println("未找到serviceId为：" + serviceId + " 的实例");
            return list;
        }
        for (ServiceInstance instance : list) {
            System.out.println("");
            System.out.println("******************************");
            System.out.println("服务实例信息：");
            System.out.println("服务 ServiceId：" + instance.getServiceId());
            System.out.println("服务 Host：" + instance.getHost());
            System.out.println("服务 Port：" + instance.getPort());
            System.out.println("服务 Uri：" + instance.getUri().toString());
            System.out.println("服务 Metadata：" + instance.getMetadata().toString());
            System.out.println("******************************");
            System.out.println("");
        }
        System.out.println("--------------------------------------");
        return list;
    }

    @GetMapping("/getServices")
    public List<String> getServices() {
        List<String> list = discoveryClient.getServices();
        System.out.println("------------------------------------");
        if (CollectionUtils.isEmpty(list)) {
            System.out.println("注册中心无服务实例");
            return list;
        }

        for (String serviceId : list) {
            System.out.println("服务Id : " + serviceId);
        }
        return list;
    }

}
