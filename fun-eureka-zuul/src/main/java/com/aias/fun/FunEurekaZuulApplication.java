package com.aias.fun;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @author liuhy
 * @since 2021/5/18
 */
@SpringCloudApplication
@EnableDiscoveryClient
@EnableZuulProxy
public class FunEurekaZuulApplication {
    public static void main(String[] args) {
        SpringApplication.run(FunEurekaZuulApplication.class, args);
    }
}
