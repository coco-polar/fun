package com.aias.fun.controller;

import cn.hutool.core.util.StrUtil;
import io.jsonwebtoken.Jwts;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;

/**
 * @author liuhy
 * @since 2021/5/25
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @GetMapping("/current")
    public Object getCurrentUser(Authentication authentication, @RequestHeader("Authorization") String header) {
        String token = StrUtil.subAfter(header, "Bearer ", false);
        return Jwts.parser()
                   .setSigningKey("test_key".getBytes(StandardCharsets.UTF_8))
                   .parseClaimsJws(token)
                   .getBody();
    }
}
