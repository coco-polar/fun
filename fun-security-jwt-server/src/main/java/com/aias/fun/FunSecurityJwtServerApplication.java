package com.aias.fun;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * @author liuhy
 * @since 2021/5/18
 */
@SpringCloudApplication
public class FunSecurityJwtServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(FunSecurityJwtServerApplication.class, args);
    }
}
