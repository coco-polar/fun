package com.aias.fun;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author liuhy
 * @since 2021/5/18
 */
@SpringCloudApplication
@EnableDiscoveryClient
@EnableAdminServer
public class FunEurekaAdminSecurityServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(FunEurekaAdminSecurityServerApplication.class, args);
    }
}
