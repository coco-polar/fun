package com.aias.fun;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Aias
 */
@EnableDiscoveryClient
@SpringCloudApplication
public class FunOauth2ApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(FunOauth2ApiApplication.class, args);
    }

}
