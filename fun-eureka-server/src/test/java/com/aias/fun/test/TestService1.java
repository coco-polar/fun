package com.aias.fun.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author liuhy
 * @since 2021/8/26
 */
@Service
public class TestService1 {

    //    @Autowired
    private TestService2 service2;

//    public TestService1(TestService2 service2) {
//        this.service2 = service2;
//    }

    @Autowired
    public void setService2(TestService2 service2) {
        this.service2 = service2;
    }

    public void doSome() {
        service2.doSome();
    }
}
