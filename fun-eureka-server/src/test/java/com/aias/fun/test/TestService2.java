package com.aias.fun.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author liuhy
 * @since 2021/8/26
 */
@Service
public class TestService2 {

    //    @Autowired
    private TestService1 service1;

    @Autowired
    public void setService1(TestService1 service1) {
        this.service1 = service1;
    }

    public void doSome() {
        System.out.println("service2");
    }
}
