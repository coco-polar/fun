package com.aias.fun.test;

import com.aias.fun.FunEurekaServerApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author liuhy
 * @since 2021/8/26
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = FunEurekaServerApplication.class)
public class EurekaServerApplicationTest {

    @Autowired
    private TestService1 service1;

    @Test
    public void test() {
        service1.doSome();
    }
}
