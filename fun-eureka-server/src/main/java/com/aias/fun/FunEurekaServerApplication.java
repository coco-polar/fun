package com.aias.fun;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author liuhy
 * @since 2021/5/18
 */
@SpringCloudApplication
@EnableEurekaServer
public class FunEurekaServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(FunEurekaServerApplication.class, args);
    }
}
