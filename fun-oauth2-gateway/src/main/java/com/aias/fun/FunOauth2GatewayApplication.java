package com.aias.fun;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author liuhy
 * @since 2021/5/27
 */
@EnableDiscoveryClient
@SpringCloudApplication
public class FunOauth2GatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(FunOauth2GatewayApplication.class, args);
    }
}
